open ASD
open Token

(* p? *)
let opt p = parser
  | [< x = p >] -> Some x
  | [<>] -> None

(* p* *)
let rec many p = parser
  | [< x = p; l = many p >] -> x :: l
  | [<>] -> []

(* p+ *)
let some p = parser
  | [< x = p; l = many p >] -> x :: l

(* p (sep p)* *)
let rec list1 p sep = parser
  | [< x = p; l = list1_aux p sep >] -> x :: l
and list1_aux p sep = parser
  | [< _ = sep; l = list1 p sep >] -> l
  | [<>] -> []

(* (p (sep p)* )? *)
let list0 p sep = parser
  | [< l = list1 p sep >] -> l
  | [<>] -> []


(* TODO : change when you extend the language *)
let rec program = parser
  | [< e = many declaration_funct; _ = Stream.empty ?? "unexpected input at the end" >] -> Program e

(* INSTRUCTION *)
and instruction = parser
  | [< 'LB; d = many declarations; i = some instruction; 'RB >] -> BlockInstruction (d,i)
  | [< e = if_then >] -> e
  | [< 'WHILE_KW; cond = expression; 'DO_KW; e = instruction; 'OD_KW >] -> WhileInstruction (cond, e)
  | [< 'DO_KW; e = instruction; 'WHILE_KW; cond = expression; 'OD_KW >] -> DoWhileInstruction (e, cond)
  | [< 'FOR_KW; 'LP; name = variable_int; 'COM; start = expression; 'COM; stop = expression; 'RP; 'DO_KW; e = instruction; 'OD_KW >] -> ForInstruction (name, start, stop, e)
  | [< 'IDENT s; e = assign_or_call s >] -> e
  | [< 'RETURN_KW; e = expression >] -> ReturnInstruction e
  | [< 'READ_KW;   e = list1 variable comma >]   -> ReadInstruction e
  | [< 'PRINT_KW;  s = list1 print_args comma >] -> PrintInstruction s

and assign_or_call s = parser
  | [< 'LP; args = list0 expression comma; 'RP >] -> CallInstruction (CallExpression (s, args, Type_Void))
  | [< v = opt variable_array_compl; 'ASSIGN; e = expression >] -> AssignInstruction (Variable (s, v), e)

and print_args = parser
  | [< 'TEXT s >] -> StringExpression s
  | [< e = expression>] -> e

(* IF THEN ELSE *)
and if_then = parser
  | [< 'IF_KW; e = expression; 'THEN_KW; e1 = instruction; e2 = opt else_part; 'FI_KW >] -> IfInstruction(e, e1, e2)
and else_part = parser
  | [< 'ELSE_KW; e = instruction >] -> e

(* DECLARATION *)
and declarations = parser
  | [<'INT_KW; e = list1 declaration_variable comma >] -> Declarations e
and declaration_variable = parser
  | [< e = variable >] -> VariableDeclaration e
and declaration_funct = parser
  | [< 'FUNC_KW;  typ=return_type; 'IDENT s; 'LP; param=list0 variable_int comma; 'RP; i=instruction >] -> FunctionDeclaration(typ, s, param, i) 
  | [< 'PROTO_KW; typ=return_type; 'IDENT s; 'LP; param=list0 variable_int comma; 'RP >] -> ProtoDeclaration(typ, s, param)

(* EXPRESSION *)
and expression = parser
  | [< e = expression_arithmetic >] -> e

(* SIMPLE EXPRESSION *)
and expression_arithmetic = parser
  | [< e1 = factor ; e = expression_low_priority e1 >] -> e

and expression_low_priority e1 = parser
  | [< 'PLUS; e2 = factor; e3 = expression_high_priority e2; e = expression_low_priority (AddExpression (e1, e3)) >] -> e
  | [< 'MINUS; e2 = factor; e3 = expression_high_priority e2; e = expression_low_priority (SubExpression (e1, e3)) >] -> e
  | [< e = expression_high_priority e1 >] -> e
  | [<>] -> e1

and expression_high_priority e1 = parser
  | [< 'MUL; e2 = factor; e = expression_low_priority (MulExpression (e1, e2)) >] -> e
  | [< 'DIV; e2 = factor; e = expression_low_priority (DivExpression (e1, e2)) >] -> e
  | [<>] -> e1

and factor = parser
  | [< e1 = primary; e = factor_aux e1 >] -> e

and factor_aux e1 = parser
  | [<>] -> e1
  | [< 'EOF >] -> e1

and primary = parser
  | [< 'INTEGER x >] -> IntegerExpression x
  | [< 'LP; e1 = factor; e = expression_low_priority e1; 'RP; e2 = primary_ternaire e >] -> e2
  | [< 'IDENT s; e = funct_var s >] -> e

and funct_var s = parser
  | [< 'LP; args = list0 expression comma; 'RP >] -> CallExpression (s, args, Type_Int)
  | [< 'LC; e = expression; 'RC >] -> LoadExpression(Variable(s, Some e))
  | [< >] -> LoadExpression(Variable(s, None))

and primary_ternaire e = parser
  | [< 'Q_MARK; expr_if = expression; 'COLON; expr_else = expression >] -> TernaireExpression(e, expr_if, expr_else)
  | [< >] -> e

and comma = parser
  | [< 'COM >] -> ()

and variable = parser
  | [< 'IDENT s; e = opt variable_array_compl >] -> Variable (s, e)
and variable_int = parser
  | [< 'IDENT s >] -> Variable (s, None)
and variable_array_compl = parser
  | [< 'LC; e = expression; 'RC >] -> e

and text = parser
  | [< 'TEXT s >] -> s

and return_type = parser
  | [< 'VOID_KW >] -> Type_Void
  | [< 'INT_KW >] ->  Type_Int