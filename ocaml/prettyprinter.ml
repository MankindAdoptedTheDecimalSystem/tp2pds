open ASD

(* main function. return only a string *)
let rec prettyprint prog =
  match prog with
    | Program d -> 
      let rec aux_decl l =
        match l with
        | [] -> ""
        | head::[] -> prettyprint_declaration head
        | head::tail -> prettyprint_declaration head ^ "\n" ^ aux_decl tail
      in 
      aux_decl d

and prettyprint_instruction instr =
  match instr with
  | BlockInstruction (d, i) -> 
    let rec aux_decl l =
      match l with
      | [] -> ""
      | head::tail -> prettyprint_declaration head ^ "\n" ^ aux_decl tail
    in 
    let rec aux_instr l =
      match l with
      | [] -> ""
      | head::[] -> prettyprint_instruction head
      | head::tail -> prettyprint_instruction head ^ "\n" ^ aux_instr tail
    in 
    "\n{\n" ^ (aux_decl d) ^ (aux_instr i) ^ "\n}"
  | AssignInstruction (name, value) -> (prettyprint_variable name) ^ " := " ^ (prettyprint_expression value)
  | WhileInstruction (cond, e) -> "WHILE " ^ prettyprint_expression cond ^ " DO " ^ prettyprint_instruction e ^ "\nDONE"
  | DoWhileInstruction (e, cond) -> " DO " ^ prettyprint_instruction e ^ "WHILE " ^ prettyprint_expression cond ^ "\nDONE"
  | ForInstruction (name, start, stop, e) -> "FOR (" ^ prettyprint_variable name ^ ", " ^ prettyprint_expression start ^ ", " ^ prettyprint_expression stop ^ ") DO " ^ prettyprint_instruction e ^ "\nDONE"
  | IfInstruction (eif, ethen, eelse) ->
    let peelse = match eelse with
    | Some e -> " ELSE " ^ (prettyprint_instruction e)
    | None -> "" in
    "IF " ^ (prettyprint_expression eif) ^ " THEN " ^ (prettyprint_instruction ethen) ^ peelse ^ " FI"
  | ReturnInstruction e -> "RETURN " ^ prettyprint_expression e
  | CallInstruction e -> prettyprint_expression e
  | PrintInstruction s -> "PRINT " ^ prettyprint_list_expression ", " s
  | ReadInstruction s -> 
    let rec aux l =
      match l with
      | [] -> ""
      | head::[] -> prettyprint_variable head
      | head::tail -> prettyprint_variable head ^ ", " ^ aux tail
    in
    "READ " ^ aux s
  
and prettyprint_expression expr =
  match expr with
  | IntegerExpression i -> string_of_int i
  | StringExpression s -> "\"" ^ s ^ "\""
  | CallExpression (funct, args, b) -> funct ^ "(" ^ prettyprint_list_expression ", " args ^ ")"
  | AddExpression (l, r) -> "(" ^ (prettyprint_expression l) ^ " + " ^ (prettyprint_expression r) ^ ")"
  | SubExpression (l, r) -> "(" ^ (prettyprint_expression l) ^ " - " ^ (prettyprint_expression r) ^ ")"
  | MulExpression (l, r) -> "(" ^ (prettyprint_expression l) ^ " * " ^ (prettyprint_expression r) ^ ")"
  | DivExpression (l, r) -> "(" ^ (prettyprint_expression l) ^ " / " ^ (prettyprint_expression r) ^ ")"
  | LoadExpression v -> prettyprint_variable v
  | TernaireExpression (cond, expr_if, expr_else) -> "(" ^ prettyprint_expression cond ^ ")? " ^ prettyprint_expression expr_if ^ " : " ^ prettyprint_expression expr_else  

and prettyprint_type t =
  match t with
  | Type_Void -> "VOID"
  | Type_Int -> "INT"
  | Type_Int_Array s -> "INT [" ^ string_of_int s ^ "]"

and prettyprint_declaration d =
  match d with
  | Declarations d_list -> 
    let rec aux l =
      match l with
      | [] -> ""
      | head::[] -> prettyprint_declaration head
      | head::tail -> prettyprint_declaration head ^ "\n" ^ aux tail
    in
    aux d_list
  | VariableDeclaration v -> "INT " ^ (prettyprint_variable v)
  | FunctionDeclaration (t, s, param, i) -> "FUNC "  ^ s ^ " (" ^ prettyprint_param param ^ ")" ^ prettyprint_instruction i
  | ProtoDeclaration    (t, s, param)    -> "PROTO " ^ s ^ " (" ^ prettyprint_param param ^ ")"

and prettyprint_variable v =
  match v with
  | Variable (id,size) -> 
    let r = match size with
      | Some s -> id ^ "[" ^ prettyprint_expression s ^ "]"
      | None -> id
    in r

and prettyprint_list_expression set param = 
let s = ref "" in
  if (List.length param) > 0 then (
    for i = 0 to (List.length param) - 2 do
      s := !s ^ prettyprint_expression (List.nth param i) ^ set
    done;
      s := !s ^ prettyprint_expression (List.nth param ((List.length param) - 1));
    !s)
  else 
    !s

and prettyprint_param param = 
  let s = ref "" in
  if (List.length param) > 0 then (
    for i = 0 to (List.length param) - 2 do
      s := !s ^ prettyprint_variable (List.nth param i) ^ ", "
    done;
      s := !s ^ prettyprint_variable (List.nth param ((List.length param) - 1));
    !s)
  else 
    !s

and prettyprint_list_instr sep e =
  let s = ref "" in
  if (List.length e) > 0 then (
    for i = 0 to (List.length e) - 2 do
      s := !s ^ prettyprint (List.nth e i) ^ sep
    done;
      s := !s ^ prettyprint (List.nth e ((List.length e) - 1));
    !s)
  else 
    !s
  ;;