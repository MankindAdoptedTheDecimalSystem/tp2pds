open List
open ASD

(* This file contains the symbol table definition. *)
(* A symbol table contains a set of ident and the  *)
(* corresponding symbols.                          *)
(* The order is important: this first match count  *)

type function_symbol_state = Defined | Declared

type function_symbol = {
  return_type: typ;
  identifier: ident;
  arguments: symbol_table;
  state: function_symbol_state;
}

and symbol =
  | VariableSymbol of typ * ident * llvm_ident
  | FunctionSymbol of function_symbol

and symbol_table = symbol list


(* Public interface *)
let lookup tab id =
  let rec assoc key = function (* like List.assoc, but with deep hidden keys *)
    | ((VariableSymbol (_, id, _)) as r) :: q
    | (FunctionSymbol {identifier = id; _} as r) :: q ->
        if key = id then
          Some r
        else
          assoc key q
    | [] -> None
  in assoc id tab

let add tab sym = sym :: tab

(* Note : obviously not symmetric *)
let merge = (@)

let is_function_defined tab id =
  match (lookup tab id) with
  | Some (FunctionSymbol _) -> ()
  | _ -> failwith ("Undeclared function: " ^ id ^".")

let is_function_correct_return_type tab id typ =
  match (lookup tab id) with
  | Some (FunctionSymbol {return_type = t; _}) ->
      if (typ != t) then
        failwith ("Function \"" ^ id ^ "\" do not have expected return type")
      else
        ()
  | _ -> ()

let is_function_call_correct tab id n =
  match (lookup tab id) with
  | Some (FunctionSymbol {arguments = args; _}) ->
      if (List.length args) != n then
        failwith ("Function \"" ^ id ^ "\" takes " ^ string_of_int (List.length args) ^ " arguments, but you give it " ^ string_of_int n ^ " arguments")
      else
        ()
  | _ -> ()

let is_function_already_declared tab id typ n = 
  match (lookup tab id) with
  | Some (FunctionSymbol {state = s; return_type = t; arguments = args; _}) ->
      if (s == Defined && (List.length args) == n) then
        failwith ("Function \"" ^ id ^ "\" is already defined.")
      else if (s == Declared && ((List.length args) != n || typ != t)) then
        failwith ("Function \"" ^ id ^ "\" do not respect its proto")
      else
        () 
  | _ -> ()

let all_proto_are_defined tab =
  let rec aux t l =
    match t with
    | (FunctionSymbol {identifier = id; state = state; _}) :: q ->
        if (state = Declared) then
          if not (List.mem id l) then
            failwith ("Prototype \"" ^ id ^ "\" is never defined")
          else
            aux q l
        else
          aux q (id::l)
    | ((VariableSymbol (_, id, _))) :: q -> aux q l
    | [] -> ()
  in
  aux tab []