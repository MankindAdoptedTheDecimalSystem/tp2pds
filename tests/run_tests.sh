#!/usr/bin/env bash

COMP=$1
if [ -z "$COMP" ]; then
  COMP=../ocaml/compile
fi

score=0
max=0
compiler_errors=0

base=`dirname $0`
for d in "$base/testlevel1" "$base/testlevel2" "$base/testlevel3" "$base/testlevel4" "$base/teststring"; do
  for f in "$d"/*.vsl; do
    max=`expr $max + 1`;
    echo $f
    $COMP "$f"
    result=$?
    if [ $result == 0 ] ; then
       rm -f out
       nm="`dirname $f`/`basename $f .vsl`"
       if [ -f "$nm.test_in" ] ; then
          timeout 1s $nm < $nm.test_in > out;
          result=$?       
       else
          timeout 1s $nm > out;
          result=$?
       fi
       if [ $result == 124 ] ; then
          echo "  FAIL: execution time limit execeeded for $f"
       else
          if cmp --quiet out $nm.out; then
            score=`expr $score + 1`;
          else
            echo "  FAIL: incorrect output on $f"
          fi
       fi
    else
      echo "  FAIL: Compilation failed for $f"
      compiler_errors=`expr $compiler_errors + 1`;
    fi
  done
done

for d in "$base/testlevelerror" "$base/teststringerror"; do
  for f in "$d"/*.vsl; do
    max=`expr $max + 1`;
    echo $f
    $COMP "$f"
    result=$?
    if [ $result == 0 ] ; then
       echo "FAIL: compilation of $f succeeded but it should fail"
    else
       score=`expr $score + 1`
    fi
  done
done

rm -f out

echo
percent=`expr 100 \* $score / $max`;
echo "Score: $score / $max tests ($percent%)"
echo "Compiler errors: $compiler_errors / $max tests"