; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.str1 = global [2 x i8] c"+\00"
@.str2 = global [4 x i8] c" = \00"
@.str3 = global [2 x i8] c"\0A\00"
@.fmt4 = global [13 x i8] c"%d%s%d%s%d%s\00"
@.str5 = global [2 x i8] c"-\00"
@.str6 = global [4 x i8] c" = \00"
@.str7 = global [2 x i8] c"\0A\00"
@.fmt8 = global [13 x i8] c"%d%s%d%s%d%s\00"
@.str9 = global [2 x i8] c"*\00"
@.str10 = global [4 x i8] c" = \00"
@.str11 = global [2 x i8] c"\0A\00"
@.fmt12 = global [13 x i8] c"%d%s%d%s%d%s\00"
@.str13 = global [2 x i8] c"/\00"
@.str14 = global [4 x i8] c" = \00"
@.str15 = global [2 x i8] c"\0A\00"
@.fmt16 = global [13 x i8] c"%d%s%d%s%d%s\00"
@.str17 = global [2 x i8] c"+\00"
@.str18 = global [4 x i8] c" = \00"
@.str19 = global [2 x i8] c"\0A\00"
@.fmt20 = global [13 x i8] c"%d%s%d%s%d%s\00"
@.str21 = global [4 x i8] c"* (\00"
@.str22 = global [2 x i8] c"+\00"
@.str23 = global [5 x i8] c") = \00"
@.str24 = global [2 x i8] c"\0A\00"
@.fmt25 = global [17 x i8] c"%d%s%d%s%d%s%d%s\00"
@.str26 = global [4 x i8] c"*  \00"
@.str27 = global [2 x i8] c"+\00"
@.str28 = global [5 x i8] c"  = \00"
@.str29 = global [2 x i8] c"\0A\00"
@.fmt30 = global [17 x i8] c"%d%s%d%s%d%s%d%s\00"


define void @main() {
%tmp1 = getelementptr [2 x i8], [2 x i8]* @.str1, i64 0, i32 0
%tmp2 = getelementptr [4 x i8], [4 x i8]* @.str2, i64 0, i32 0
%tmp3 = add i32 5, 7
%tmp4 = getelementptr [2 x i8], [2 x i8]* @.str3, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.fmt4, i64 0, i64 0), i32 5, i8* %tmp1, i32 7, i8* %tmp2, i32 %tmp3, i8* %tmp4)
%tmp5 = getelementptr [2 x i8], [2 x i8]* @.str5, i64 0, i32 0
%tmp6 = getelementptr [4 x i8], [4 x i8]* @.str6, i64 0, i32 0
%tmp7 = sub i32 5, 7
%tmp8 = getelementptr [2 x i8], [2 x i8]* @.str7, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.fmt8, i64 0, i64 0), i32 5, i8* %tmp5, i32 7, i8* %tmp6, i32 %tmp7, i8* %tmp8)
%tmp9 = getelementptr [2 x i8], [2 x i8]* @.str9, i64 0, i32 0
%tmp10 = getelementptr [4 x i8], [4 x i8]* @.str10, i64 0, i32 0
%tmp11 = mul i32 5, 7
%tmp12 = getelementptr [2 x i8], [2 x i8]* @.str11, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.fmt12, i64 0, i64 0), i32 5, i8* %tmp9, i32 7, i8* %tmp10, i32 %tmp11, i8* %tmp12)
%tmp13 = getelementptr [2 x i8], [2 x i8]* @.str13, i64 0, i32 0
%tmp14 = getelementptr [4 x i8], [4 x i8]* @.str14, i64 0, i32 0
%tmp15 = sdiv i32 5, 7
%tmp16 = getelementptr [2 x i8], [2 x i8]* @.str15, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.fmt16, i64 0, i64 0), i32 5, i8* %tmp13, i32 7, i8* %tmp14, i32 %tmp15, i8* %tmp16)
%tmp17 = getelementptr [2 x i8], [2 x i8]* @.str17, i64 0, i32 0
%tmp18 = getelementptr [4 x i8], [4 x i8]* @.str18, i64 0, i32 0
%tmp19 = add i32 5, 1
%tmp20 = getelementptr [2 x i8], [2 x i8]* @.str19, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.fmt20, i64 0, i64 0), i32 5, i8* %tmp17, i32 1, i8* %tmp18, i32 %tmp19, i8* %tmp20)
%tmp21 = getelementptr [4 x i8], [4 x i8]* @.str21, i64 0, i32 0
%tmp22 = getelementptr [2 x i8], [2 x i8]* @.str22, i64 0, i32 0
%tmp23 = getelementptr [5 x i8], [5 x i8]* @.str23, i64 0, i32 0
%tmp24 = add i32 5, 7
%tmp25 = mul i32 5, %tmp24
%tmp26 = getelementptr [2 x i8], [2 x i8]* @.str24, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.fmt25, i64 0, i64 0), i32 5, i8* %tmp21, i32 5, i8* %tmp22, i32 7, i8* %tmp23, i32 %tmp25, i8* %tmp26)
%tmp27 = getelementptr [4 x i8], [4 x i8]* @.str26, i64 0, i32 0
%tmp28 = getelementptr [2 x i8], [2 x i8]* @.str27, i64 0, i32 0
%tmp29 = getelementptr [5 x i8], [5 x i8]* @.str28, i64 0, i32 0
%tmp30 = mul i32 5, 5
%tmp31 = add i32 %tmp30, 7
%tmp32 = getelementptr [2 x i8], [2 x i8]* @.str29, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.fmt30, i64 0, i64 0), i32 5, i8* %tmp27, i32 5, i8* %tmp28, i32 7, i8* %tmp29, i32 %tmp31, i8* %tmp32)
ret void
}

