; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins


define i32 @main() {
br label %while1

while1:
%tmp1 = icmp ne i32 1, 0
br i1 %tmp1, label %do2, label %done3

do2:
ret i32 0
br label %while1

done3:
ret i32 0
}

