; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.str1 = global [9 x i8] c"one() = \00"
@.fmt2 = global [5 x i8] c"%s%d\00"


define void @main() {
%tmp1 = getelementptr [9 x i8], [9 x i8]* @.str1, i64 0, i32 0
%tmp2 = call i32 @one()
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.fmt2, i64 0, i64 0), i8* %tmp1, i32 %tmp2)
ret void
}
define i32 @one() {
ret i32 1
ret i32 0
}

