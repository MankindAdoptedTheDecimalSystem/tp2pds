; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.str1 = global [2 x i8] c"+\00"
@.str2 = global [4 x i8] c" = \00"
@.str3 = global [2 x i8] c"\0A\00"
@.fmt4 = global [13 x i8] c"%d%s%d%s%d%s\00"
@.str5 = global [2 x i8] c"-\00"
@.str6 = global [4 x i8] c" = \00"
@.str7 = global [2 x i8] c"\0A\00"
@.fmt8 = global [13 x i8] c"%d%s%d%s%d%s\00"
@.str9 = global [2 x i8] c"*\00"
@.str10 = global [4 x i8] c" = \00"
@.str11 = global [2 x i8] c"\0A\00"
@.fmt12 = global [13 x i8] c"%d%s%d%s%d%s\00"
@.str13 = global [2 x i8] c"/\00"
@.str14 = global [4 x i8] c" = \00"
@.str15 = global [2 x i8] c"\0A\00"
@.fmt16 = global [13 x i8] c"%d%s%d%s%d%s\00"
@.str17 = global [2 x i8] c"+\00"
@.str18 = global [4 x i8] c" = \00"
@.str19 = global [2 x i8] c"\0A\00"
@.fmt20 = global [13 x i8] c"%d%s%d%s%d%s\00"
@.str21 = global [4 x i8] c"* (\00"
@.str22 = global [2 x i8] c"+\00"
@.str23 = global [5 x i8] c") = \00"
@.str24 = global [2 x i8] c"\0A\00"
@.fmt25 = global [17 x i8] c"%d%s%d%s%d%s%d%s\00"
@.str26 = global [4 x i8] c"*  \00"
@.str27 = global [2 x i8] c"+\00"
@.str28 = global [5 x i8] c"  = \00"
@.str29 = global [2 x i8] c"\0A\00"
@.fmt30 = global [17 x i8] c"%d%s%d%s%d%s%d%s\00"


define void @main() {
call void @expr(i32 1, i32 3)
call void @expr(i32 5, i32 2)
ret void
}
define void @expr(i32 %tmp2, i32 %tmp4) {
%tmp1 = alloca i32
store i32 %tmp2, i32* %tmp1
%tmp3 = alloca i32
store i32 %tmp4, i32* %tmp3
%tmp5 = load i32, i32* %tmp1
%tmp6 = getelementptr [2 x i8], [2 x i8]* @.str1, i64 0, i32 0
%tmp7 = load i32, i32* %tmp3
%tmp8 = getelementptr [4 x i8], [4 x i8]* @.str2, i64 0, i32 0
%tmp9 = load i32, i32* %tmp1
%tmp10 = load i32, i32* %tmp3
%tmp11 = add i32 %tmp9, %tmp10
%tmp12 = getelementptr [2 x i8], [2 x i8]* @.str3, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.fmt4, i64 0, i64 0), i32 %tmp5, i8* %tmp6, i32 %tmp7, i8* %tmp8, i32 %tmp11, i8* %tmp12)
%tmp13 = load i32, i32* %tmp1
%tmp14 = getelementptr [2 x i8], [2 x i8]* @.str5, i64 0, i32 0
%tmp15 = load i32, i32* %tmp3
%tmp16 = getelementptr [4 x i8], [4 x i8]* @.str6, i64 0, i32 0
%tmp17 = load i32, i32* %tmp1
%tmp18 = load i32, i32* %tmp3
%tmp19 = sub i32 %tmp17, %tmp18
%tmp20 = getelementptr [2 x i8], [2 x i8]* @.str7, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.fmt8, i64 0, i64 0), i32 %tmp13, i8* %tmp14, i32 %tmp15, i8* %tmp16, i32 %tmp19, i8* %tmp20)
%tmp21 = load i32, i32* %tmp1
%tmp22 = getelementptr [2 x i8], [2 x i8]* @.str9, i64 0, i32 0
%tmp23 = load i32, i32* %tmp3
%tmp24 = getelementptr [4 x i8], [4 x i8]* @.str10, i64 0, i32 0
%tmp25 = load i32, i32* %tmp1
%tmp26 = load i32, i32* %tmp3
%tmp27 = mul i32 %tmp25, %tmp26
%tmp28 = getelementptr [2 x i8], [2 x i8]* @.str11, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.fmt12, i64 0, i64 0), i32 %tmp21, i8* %tmp22, i32 %tmp23, i8* %tmp24, i32 %tmp27, i8* %tmp28)
%tmp29 = load i32, i32* %tmp1
%tmp30 = getelementptr [2 x i8], [2 x i8]* @.str13, i64 0, i32 0
%tmp31 = load i32, i32* %tmp3
%tmp32 = getelementptr [4 x i8], [4 x i8]* @.str14, i64 0, i32 0
%tmp33 = load i32, i32* %tmp1
%tmp34 = load i32, i32* %tmp3
%tmp35 = sdiv i32 %tmp33, %tmp34
%tmp36 = getelementptr [2 x i8], [2 x i8]* @.str15, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.fmt16, i64 0, i64 0), i32 %tmp29, i8* %tmp30, i32 %tmp31, i8* %tmp32, i32 %tmp35, i8* %tmp36)
%tmp37 = load i32, i32* %tmp1
%tmp38 = getelementptr [2 x i8], [2 x i8]* @.str17, i64 0, i32 0
%tmp39 = getelementptr [4 x i8], [4 x i8]* @.str18, i64 0, i32 0
%tmp40 = load i32, i32* %tmp1
%tmp41 = add i32 %tmp40, 1
%tmp42 = getelementptr [2 x i8], [2 x i8]* @.str19, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.fmt20, i64 0, i64 0), i32 %tmp37, i8* %tmp38, i32 1, i8* %tmp39, i32 %tmp41, i8* %tmp42)
%tmp43 = load i32, i32* %tmp1
%tmp44 = getelementptr [4 x i8], [4 x i8]* @.str21, i64 0, i32 0
%tmp45 = load i32, i32* %tmp1
%tmp46 = getelementptr [2 x i8], [2 x i8]* @.str22, i64 0, i32 0
%tmp47 = load i32, i32* %tmp3
%tmp48 = getelementptr [5 x i8], [5 x i8]* @.str23, i64 0, i32 0
%tmp49 = load i32, i32* %tmp1
%tmp50 = load i32, i32* %tmp1
%tmp51 = load i32, i32* %tmp3
%tmp52 = add i32 %tmp50, %tmp51
%tmp53 = mul i32 %tmp49, %tmp52
%tmp54 = getelementptr [2 x i8], [2 x i8]* @.str24, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.fmt25, i64 0, i64 0), i32 %tmp43, i8* %tmp44, i32 %tmp45, i8* %tmp46, i32 %tmp47, i8* %tmp48, i32 %tmp53, i8* %tmp54)
%tmp55 = load i32, i32* %tmp1
%tmp56 = getelementptr [4 x i8], [4 x i8]* @.str26, i64 0, i32 0
%tmp57 = load i32, i32* %tmp1
%tmp58 = getelementptr [2 x i8], [2 x i8]* @.str27, i64 0, i32 0
%tmp59 = load i32, i32* %tmp3
%tmp60 = getelementptr [5 x i8], [5 x i8]* @.str28, i64 0, i32 0
%tmp61 = load i32, i32* %tmp1
%tmp62 = load i32, i32* %tmp1
%tmp63 = mul i32 %tmp61, %tmp62
%tmp64 = load i32, i32* %tmp3
%tmp65 = add i32 %tmp63, %tmp64
%tmp66 = getelementptr [2 x i8], [2 x i8]* @.str29, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.fmt30, i64 0, i64 0), i32 %tmp55, i8* %tmp56, i32 %tmp57, i8* %tmp58, i32 %tmp59, i8* %tmp60, i32 %tmp65, i8* %tmp66)
ret void
}

