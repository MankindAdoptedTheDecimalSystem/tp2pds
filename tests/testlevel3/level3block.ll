; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.fmt1 = global [3 x i8] c"%d\00"
@.fmt2 = global [3 x i8] c"%d\00"
@.str3 = global [22 x i8] c"y a l'interieur vaut \00"
@.fmt4 = global [5 x i8] c"%s%d\00"
@.str5 = global [38 x i8] c", mais a l'exterieur du bloc il vaut \00"
@.fmt6 = global [5 x i8] c"%s%d\00"


define void @main() {
%tmp1 = alloca i32
call i32 (i8*, ...) @scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt1, i64 0, i64 0), i32* %tmp1)
%tmp2 = alloca i32
call i32 (i8*, ...) @scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt2, i64 0, i64 0), i32* %tmp2)
%tmp3 = getelementptr [22 x i8], [22 x i8]* @.str3, i64 0, i32 0
%tmp4 = load i32, i32* %tmp2
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.fmt4, i64 0, i64 0), i8* %tmp3, i32 %tmp4)
%tmp5 = getelementptr [38 x i8], [38 x i8]* @.str5, i64 0, i32 0
%tmp6 = load i32, i32* %tmp1
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.fmt6, i64 0, i64 0), i8* %tmp5, i32 %tmp6)
ret void
}

