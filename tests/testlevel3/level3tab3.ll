; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.fmt1 = global [3 x i8] c"%d\00"
@.str2 = global [3 x i8] c"t[\00"
@.str3 = global [5 x i8] c"] = \00"
@.str4 = global [2 x i8] c"\0A\00"
@.fmt5 = global [11 x i8] c"%s%d%s%d%s\00"


define void @main() {
%tmp1 = alloca i32
%tmp2 = alloca [8 x i32]
%tmp3 = alloca i32
store i32 0, i32* %tmp1
br label %while1

while1:
%tmp4 = load i32, i32* %tmp1
%tmp5 = sub i32 8, %tmp4
%tmp11 = icmp ne i32 %tmp5, 0
br i1 %tmp11, label %do2, label %done3

do2:
call i32 (i8*, ...) @scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt1, i64 0, i64 0), i32* %tmp3)
%tmp6 = load i32, i32* %tmp1
%tmp7 = getelementptr [8 x i32], [8 x i32]* %tmp2, i64 0, i32 %tmp6
%tmp8 = load i32, i32* %tmp3
store i32 %tmp8, i32* %tmp7
%tmp9 = load i32, i32* %tmp1
%tmp10 = add i32 %tmp9, 1
store i32 %tmp10, i32* %tmp1
br label %while1

done3:
store i32 0, i32* %tmp1
br label %while4

while4:
%tmp12 = load i32, i32* %tmp1
%tmp13 = sub i32 8, %tmp12
%tmp23 = icmp ne i32 %tmp13, 0
br i1 %tmp23, label %do5, label %done6

do5:
%tmp14 = getelementptr [3 x i8], [3 x i8]* @.str2, i64 0, i32 0
%tmp15 = load i32, i32* %tmp1
%tmp16 = getelementptr [5 x i8], [5 x i8]* @.str3, i64 0, i32 0
%tmp18 = load i32, i32* %tmp1
%tmp19 = getelementptr [8 x i32], [8 x i32]* %tmp2, i64 0, i32 %tmp18
%tmp17 = load i32, i32* %tmp19
%tmp20 = getelementptr [2 x i8], [2 x i8]* @.str4, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.fmt5, i64 0, i64 0), i8* %tmp14, i32 %tmp15, i8* %tmp16, i32 %tmp17, i8* %tmp20)
%tmp21 = load i32, i32* %tmp1
%tmp22 = add i32 %tmp21, 1
store i32 %tmp22, i32* %tmp1
br label %while4

done6:
ret void
}

