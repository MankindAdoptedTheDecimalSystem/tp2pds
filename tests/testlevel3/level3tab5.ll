; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.str1 = global [25 x i8] c"Main: Tableau de taille \00"
@.str2 = global [5 x i8] c" = [\00"
@.fmt3 = global [7 x i8] c"%s%d%s\00"
@.str4 = global [2 x i8] c",\00"
@.fmt5 = global [3 x i8] c"%s\00"
@.fmt6 = global [3 x i8] c"%d\00"
@.str7 = global [3 x i8] c"]\0A\00"
@.fmt8 = global [3 x i8] c"%s\00"
@.str9 = global [5 x i8] c"Fini\00"
@.fmt10 = global [3 x i8] c"%s\00"
@.str11 = global [11 x i8] c"entrezno :\00"
@.str12 = global [3 x i8] c"  \00"
@.fmt13 = global [7 x i8] c"%s%d%s\00"
@.fmt14 = global [3 x i8] c"%d\00"
@.str15 = global [19 x i8] c"Tableau de taille \00"
@.str16 = global [5 x i8] c" = [\00"
@.fmt17 = global [7 x i8] c"%s%d%s\00"
@.str18 = global [2 x i8] c",\00"
@.fmt19 = global [3 x i8] c"%s\00"
@.fmt20 = global [3 x i8] c"%d\00"
@.str21 = global [3 x i8] c"]\0A\00"
@.fmt22 = global [3 x i8] c"%s\00"


define void @main() {
%tmp1 = alloca i32
%tmp2 = alloca i32
%tmp3 = alloca [2 x i32]
store i32 2, i32* %tmp2
%tmp4 = load i32, i32* %tmp2
%tmp5 = getelementptr [2 x i32], [2 x i32]* %tmp3, i64 0, i32 0
call void @readprinttab(i32 %tmp4, i32* %tmp5)
%tmp6 = getelementptr [25 x i8], [25 x i8]* @.str1, i64 0, i32 0
%tmp7 = load i32, i32* %tmp2
%tmp8 = getelementptr [5 x i8], [5 x i8]* @.str2, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.fmt3, i64 0, i64 0), i8* %tmp6, i32 %tmp7, i8* %tmp8)
store i32 0, i32* %tmp1
br label %while3

while3:
%tmp9 = load i32, i32* %tmp2
%tmp10 = load i32, i32* %tmp1
%tmp11 = sub i32 %tmp9, %tmp10
%tmp20 = icmp ne i32 %tmp11, 0
br i1 %tmp20, label %do4, label %done5

do4:
%tmp12 = load i32, i32* %tmp1
%tmp13 = icmp ne i32 %tmp12, 0
br i1 %tmp13, label %then1, label %fi2

then1:
%tmp14 = getelementptr [2 x i8], [2 x i8]* @.str4, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt5, i64 0, i64 0), i8* %tmp14)
br label %fi2

fi2:
%tmp16 = load i32, i32* %tmp1
%tmp17 = getelementptr [2 x i32], [2 x i32]* %tmp3, i64 0, i32 %tmp16
%tmp15 = load i32, i32* %tmp17
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt6, i64 0, i64 0), i32 %tmp15)
%tmp18 = load i32, i32* %tmp1
%tmp19 = add i32 %tmp18, 1
store i32 %tmp19, i32* %tmp1
br label %while3

done5:
%tmp21 = getelementptr [3 x i8], [3 x i8]* @.str7, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt8, i64 0, i64 0), i8* %tmp21)
%tmp22 = getelementptr [5 x i8], [5 x i8]* @.str9, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt10, i64 0, i64 0), i8* %tmp22)
ret void
}
define void @readprinttab(i32 %tmp24, i32* %tmp26) {
%tmp23 = alloca i32
store i32 %tmp24, i32* %tmp23
%tmp25 = alloca i32*
store i32* %tmp26, i32** %tmp25
%tmp27 = alloca i32
store i32 0, i32* %tmp27
br label %while6

while6:
%tmp28 = load i32, i32* %tmp23
%tmp29 = load i32, i32* %tmp27
%tmp30 = sub i32 %tmp28, %tmp29
%tmp39 = icmp ne i32 %tmp30, 0
br i1 %tmp39, label %do7, label %done8

do7:
%tmp31 = getelementptr [11 x i8], [11 x i8]* @.str11, i64 0, i32 0
%tmp32 = load i32, i32* %tmp27
%tmp33 = getelementptr [3 x i8], [3 x i8]* @.str12, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.fmt13, i64 0, i64 0), i8* %tmp31, i32 %tmp32, i8* %tmp33)
%tmp34 = load i32, i32* %tmp27
%tmp36 = load i32*, i32** %tmp25
%tmp35 = getelementptr i32, i32* %tmp36, i32 %tmp34
call i32 (i8*, ...) @scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt14, i64 0, i64 0), i32* %tmp35)
%tmp37 = load i32, i32* %tmp27
%tmp38 = add i32 %tmp37, 1
store i32 %tmp38, i32* %tmp27
br label %while6

done8:
%tmp40 = getelementptr [19 x i8], [19 x i8]* @.str15, i64 0, i32 0
%tmp41 = load i32, i32* %tmp23
%tmp42 = getelementptr [5 x i8], [5 x i8]* @.str16, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.fmt17, i64 0, i64 0), i8* %tmp40, i32 %tmp41, i8* %tmp42)
store i32 0, i32* %tmp27
br label %while11

while11:
%tmp43 = load i32, i32* %tmp23
%tmp44 = load i32, i32* %tmp27
%tmp45 = sub i32 %tmp43, %tmp44
%tmp55 = icmp ne i32 %tmp45, 0
br i1 %tmp55, label %do12, label %done13

do12:
%tmp46 = load i32, i32* %tmp27
%tmp47 = icmp ne i32 %tmp46, 0
br i1 %tmp47, label %then9, label %fi10

then9:
%tmp48 = getelementptr [2 x i8], [2 x i8]* @.str18, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt19, i64 0, i64 0), i8* %tmp48)
br label %fi10

fi10:
%tmp50 = load i32, i32* %tmp27
%tmp52 = load i32*, i32** %tmp25
%tmp51 = getelementptr i32, i32* %tmp52, i32 %tmp50
%tmp49 = load i32, i32* %tmp51
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt20, i64 0, i64 0), i32 %tmp49)
%tmp53 = load i32, i32* %tmp27
%tmp54 = add i32 %tmp53, 1
store i32 %tmp54, i32* %tmp27
br label %while11

done13:
%tmp56 = getelementptr [3 x i8], [3 x i8]* @.str21, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt22, i64 0, i64 0), i8* %tmp56)
ret void
}

