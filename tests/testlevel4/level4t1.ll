; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.str1 = global [11 x i8] c"Et voila: \00"
@.fmt2 = global [11 x i8] c"%s%d%d%d%d\00"


define void @main() {
%tmp1 = alloca i32
%tmp2 = alloca i32
%tmp3 = alloca i32
%tmp4 = alloca i32
store i32 0, i32* %tmp1
store i32 0, i32* %tmp2
store i32 0, i32* %tmp3
store i32 0, i32* %tmp4
%tmp5 = alloca i32
%tmp6 = alloca i32
%tmp7 = alloca i32
store i32 1, i32* %tmp4
%tmp8 = alloca i32
%tmp9 = alloca i32
store i32 2, i32* %tmp7
%tmp10 = alloca i32
store i32 3, i32* %tmp9
%tmp11 = getelementptr [11 x i8], [11 x i8]* @.str1, i64 0, i32 0
%tmp12 = load i32, i32* %tmp1
%tmp13 = load i32, i32* %tmp2
%tmp14 = load i32, i32* %tmp3
%tmp15 = load i32, i32* %tmp4
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.fmt2, i64 0, i64 0), i8* %tmp11, i32 %tmp12, i32 %tmp13, i32 %tmp14, i32 %tmp15)
ret void
}

