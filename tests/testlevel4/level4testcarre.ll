; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.str1 = global [2 x i8] c"\0A\00"
@.fmt2 = global [3 x i8] c"%s\00"
@.str3 = global [6 x i8] c"^2 + \00"
@.fmt4 = global [5 x i8] c"%d%s\00"
@.str5 = global [7 x i8] c"1^2 = \00"
@.str6 = global [2 x i8] c"\0A\00"
@.fmt7 = global [7 x i8] c"%s%d%s\00"


define void @main() {
%tmp1 = alloca i32
%tmp2 = alloca i32
%tmp3 = alloca i32
store i32 5, i32* %tmp1
store i32 0, i32* %tmp3
%tmp4 = load i32, i32* %tmp1
store i32 %tmp4, i32* %tmp2
br label %while1

while1:
%tmp5 = load i32, i32* %tmp2
%tmp13 = icmp ne i32 %tmp5, 0
br i1 %tmp13, label %do2, label %done3

do2:
%tmp6 = load i32, i32* %tmp3
%tmp7 = load i32, i32* %tmp2
%tmp8 = load i32, i32* %tmp2
%tmp9 = mul i32 %tmp7, %tmp8
%tmp10 = add i32 %tmp6, %tmp9
store i32 %tmp10, i32* %tmp3
%tmp11 = load i32, i32* %tmp2
%tmp12 = sub i32 %tmp11, 1
store i32 %tmp12, i32* %tmp2
br label %while1

done3:
%tmp14 = getelementptr [2 x i8], [2 x i8]* @.str1, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt2, i64 0, i64 0), i8* %tmp14)
%tmp15 = load i32, i32* %tmp1
store i32 %tmp15, i32* %tmp2
br label %while4

while4:
%tmp16 = load i32, i32* %tmp2
%tmp17 = sub i32 %tmp16, 1
%tmp22 = icmp ne i32 %tmp17, 0
br i1 %tmp22, label %do5, label %done6

do5:
%tmp18 = load i32, i32* %tmp2
%tmp19 = getelementptr [6 x i8], [6 x i8]* @.str3, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.fmt4, i64 0, i64 0), i32 %tmp18, i8* %tmp19)
%tmp20 = load i32, i32* %tmp2
%tmp21 = sub i32 %tmp20, 1
store i32 %tmp21, i32* %tmp2
br label %while4

done6:
%tmp23 = getelementptr [7 x i8], [7 x i8]* @.str5, i64 0, i32 0
%tmp24 = load i32, i32* %tmp3
%tmp25 = getelementptr [2 x i8], [2 x i8]* @.str6, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.fmt7, i64 0, i64 0), i8* %tmp23, i32 %tmp24, i8* %tmp25)
ret void
}

