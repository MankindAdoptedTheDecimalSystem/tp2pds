; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.str1 = global [3 x i8] c"f(\00"
@.str2 = global [5 x i8] c") = \00"
@.str3 = global [2 x i8] c"\0A\00"
@.fmt4 = global [11 x i8] c"%s%d%s%d%s\00"


define void @main() {
%tmp1 = alloca i32
%tmp2 = alloca [11 x i32]
store i32 0, i32* %tmp1
br label %while1

while1:
%tmp3 = load i32, i32* %tmp1
%tmp4 = sub i32 11, %tmp3
%tmp11 = icmp ne i32 %tmp4, 0
br i1 %tmp11, label %do2, label %done3

do2:
%tmp5 = load i32, i32* %tmp1
%tmp6 = getelementptr [11 x i32], [11 x i32]* %tmp2, i64 0, i32 %tmp5
%tmp7 = load i32, i32* %tmp1
%tmp8 = call i32 @fact(i32 %tmp7)
store i32 %tmp8, i32* %tmp6
%tmp9 = load i32, i32* %tmp1
%tmp10 = add i32 %tmp9, 1
store i32 %tmp10, i32* %tmp1
br label %while1

done3:
store i32 0, i32* %tmp1
br label %while4

while4:
%tmp12 = load i32, i32* %tmp1
%tmp13 = sub i32 11, %tmp12
%tmp23 = icmp ne i32 %tmp13, 0
br i1 %tmp23, label %do5, label %done6

do5:
%tmp14 = getelementptr [3 x i8], [3 x i8]* @.str1, i64 0, i32 0
%tmp15 = load i32, i32* %tmp1
%tmp16 = getelementptr [5 x i8], [5 x i8]* @.str2, i64 0, i32 0
%tmp18 = load i32, i32* %tmp1
%tmp19 = getelementptr [11 x i32], [11 x i32]* %tmp2, i64 0, i32 %tmp18
%tmp17 = load i32, i32* %tmp19
%tmp20 = getelementptr [2 x i8], [2 x i8]* @.str3, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.fmt4, i64 0, i64 0), i8* %tmp14, i32 %tmp15, i8* %tmp16, i32 %tmp17, i8* %tmp20)
%tmp21 = load i32, i32* %tmp1
%tmp22 = add i32 %tmp21, 1
store i32 %tmp22, i32* %tmp1
br label %while4

done6:
ret void
}
define i32 @fact(i32 %tmp25) {
%tmp24 = alloca i32
store i32 %tmp25, i32* %tmp24
%tmp26 = load i32, i32* %tmp24
%tmp27 = icmp ne i32 %tmp26, 0
br i1 %tmp27, label %then7, label %else9

then7:
%tmp28 = load i32, i32* %tmp24
%tmp29 = load i32, i32* %tmp24
%tmp30 = sub i32 %tmp29, 1
%tmp31 = call i32 @fact(i32 %tmp30)
%tmp32 = mul i32 %tmp28, %tmp31
ret i32 %tmp32
br label %fi8

else9:
ret i32 1
br label %fi8

fi8:
ret i32 0
}

