; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.str1 = global [14 x i8] c"\0A Hanoi avec \00"
@.str2 = global [11 x i8] c" disques\0A\0A\00"
@.fmt3 = global [7 x i8] c"%s%d%s\00"
@.str4 = global [14 x i8] c"\0A\0AHanoi avec \00"
@.str5 = global [11 x i8] c" disques\0A\0A\00"
@.fmt6 = global [7 x i8] c"%s%d%s\00"
@.str7 = global [23 x i8] c"Deplacer un disque de \00"
@.str8 = global [4 x i8] c" a \00"
@.str9 = global [2 x i8] c"\0A\00"
@.fmt10 = global [11 x i8] c"%s%d%s%d%s\00"


define void @main() {
%tmp1 = alloca i32
%tmp2 = alloca i32
store i32 3, i32* %tmp1
%tmp3 = getelementptr [14 x i8], [14 x i8]* @.str1, i64 0, i32 0
%tmp4 = load i32, i32* %tmp1
%tmp5 = getelementptr [11 x i8], [11 x i8]* @.str2, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.fmt3, i64 0, i64 0), i8* %tmp3, i32 %tmp4, i8* %tmp5)
%tmp6 = load i32, i32* %tmp1
%tmp7 = call i32 @hanoi(i32 %tmp6, i32 1, i32 3, i32 2)
store i32 %tmp7, i32* %tmp2
store i32 4, i32* %tmp1
%tmp8 = getelementptr [14 x i8], [14 x i8]* @.str4, i64 0, i32 0
%tmp9 = load i32, i32* %tmp1
%tmp10 = getelementptr [11 x i8], [11 x i8]* @.str5, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.fmt6, i64 0, i64 0), i8* %tmp8, i32 %tmp9, i8* %tmp10)
%tmp11 = load i32, i32* %tmp1
%tmp12 = call i32 @hanoi(i32 %tmp11, i32 1, i32 3, i32 2)
store i32 %tmp12, i32* %tmp2
ret void
}
define i32 @hanoi(i32 %tmp14, i32 %tmp16, i32 %tmp18, i32 %tmp20) {
%tmp13 = alloca i32
store i32 %tmp14, i32* %tmp13
%tmp15 = alloca i32
store i32 %tmp16, i32* %tmp15
%tmp17 = alloca i32
store i32 %tmp18, i32* %tmp17
%tmp19 = alloca i32
store i32 %tmp20, i32* %tmp19
%tmp21 = alloca i32
%tmp22 = load i32, i32* %tmp13
%tmp23 = icmp ne i32 %tmp22, 0
br i1 %tmp23, label %then1, label %fi2

then1:
%tmp24 = load i32, i32* %tmp13
%tmp25 = sub i32 %tmp24, 1
%tmp26 = load i32, i32* %tmp15
%tmp27 = load i32, i32* %tmp19
%tmp28 = load i32, i32* %tmp17
%tmp29 = call i32 @hanoi(i32 %tmp25, i32 %tmp26, i32 %tmp27, i32 %tmp28)
store i32 %tmp29, i32* %tmp21
%tmp30 = getelementptr [23 x i8], [23 x i8]* @.str7, i64 0, i32 0
%tmp31 = load i32, i32* %tmp15
%tmp32 = getelementptr [4 x i8], [4 x i8]* @.str8, i64 0, i32 0
%tmp33 = load i32, i32* %tmp17
%tmp34 = getelementptr [2 x i8], [2 x i8]* @.str9, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.fmt10, i64 0, i64 0), i8* %tmp30, i32 %tmp31, i8* %tmp32, i32 %tmp33, i8* %tmp34)
%tmp35 = load i32, i32* %tmp13
%tmp36 = sub i32 %tmp35, 1
%tmp37 = load i32, i32* %tmp19
%tmp38 = load i32, i32* %tmp17
%tmp39 = load i32, i32* %tmp15
%tmp40 = call i32 @hanoi(i32 %tmp36, i32 %tmp37, i32 %tmp38, i32 %tmp39)
store i32 %tmp40, i32* %tmp21
br label %fi2

fi2:
ret i32 1
ret i32 0
}

