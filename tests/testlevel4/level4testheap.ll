; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.str1 = global [12 x i8] c"\0A Entrer le\00"
@.str2 = global [7 x i8] c"eme:  \00"
@.fmt3 = global [7 x i8] c"%s%d%s\00"
@.fmt4 = global [3 x i8] c"%d\00"
@.str5 = global [6 x i8] c"   i=\00"
@.str6 = global [5 x i8] c"  j=\00"
@.fmt7 = global [9 x i8] c"%s%d%s%d\00"
@.str8 = global [5 x i8] c"\0A t[\00"
@.str9 = global [5 x i8] c"] = \00"
@.fmt10 = global [9 x i8] c"%s%d%s%d\00"


define void @main() {
%tmp1 = alloca [10 x i32]
%tmp2 = alloca i32
%tmp3 = alloca i32
store i32 0, i32* %tmp2
br label %while1

while1:
%tmp4 = load i32, i32* %tmp2
%tmp5 = sub i32 10, %tmp4
%tmp18 = icmp ne i32 %tmp5, 0
br i1 %tmp18, label %do2, label %done3

do2:
%tmp6 = getelementptr [12 x i8], [12 x i8]* @.str1, i64 0, i32 0
%tmp7 = load i32, i32* %tmp2
%tmp8 = getelementptr [7 x i8], [7 x i8]* @.str2, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.fmt3, i64 0, i64 0), i8* %tmp6, i32 %tmp7, i8* %tmp8)
call i32 (i8*, ...) @scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt4, i64 0, i64 0), i32* %tmp3)
%tmp9 = load i32, i32* %tmp2
%tmp10 = getelementptr [10 x i32], [10 x i32]* %tmp1, i64 0, i32 %tmp9
%tmp11 = load i32, i32* %tmp3
store i32 %tmp11, i32* %tmp10
%tmp12 = load i32, i32* %tmp2
%tmp13 = add i32 %tmp12, 1
store i32 %tmp13, i32* %tmp2
%tmp14 = getelementptr [6 x i8], [6 x i8]* @.str5, i64 0, i32 0
%tmp15 = load i32, i32* %tmp2
%tmp16 = getelementptr [5 x i8], [5 x i8]* @.str6, i64 0, i32 0
%tmp17 = load i32, i32* %tmp3
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.fmt7, i64 0, i64 0), i8* %tmp14, i32 %tmp15, i8* %tmp16, i32 %tmp17)
br label %while1

done3:
%tmp19 = getelementptr [10 x i32], [10 x i32]* %tmp1, i64 0, i32 0
call void @heapsort(i32* %tmp19, i32 10)
store i32 0, i32* %tmp2
br label %while4

while4:
%tmp20 = load i32, i32* %tmp2
%tmp21 = sub i32 10, %tmp20
%tmp30 = icmp ne i32 %tmp21, 0
br i1 %tmp30, label %do5, label %done6

do5:
%tmp22 = getelementptr [5 x i8], [5 x i8]* @.str8, i64 0, i32 0
%tmp23 = load i32, i32* %tmp2
%tmp24 = getelementptr [5 x i8], [5 x i8]* @.str9, i64 0, i32 0
%tmp26 = load i32, i32* %tmp2
%tmp27 = getelementptr [10 x i32], [10 x i32]* %tmp1, i64 0, i32 %tmp26
%tmp25 = load i32, i32* %tmp27
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.fmt10, i64 0, i64 0), i8* %tmp22, i32 %tmp23, i8* %tmp24, i32 %tmp25)
%tmp28 = load i32, i32* %tmp2
%tmp29 = add i32 %tmp28, 1
store i32 %tmp29, i32* %tmp2
br label %while4

done6:
ret void
}
define void @heapsort(i32* %tmp32, i32 %tmp34) {
%tmp31 = alloca i32*
store i32* %tmp32, i32** %tmp31
%tmp33 = alloca i32
store i32 %tmp34, i32* %tmp33
%tmp35 = alloca i32
%tmp36 = alloca i32
%tmp37 = alloca i32
%tmp38 = alloca i32
%tmp39 = alloca i32
%tmp40 = load i32, i32* %tmp33
%tmp41 = sdiv i32 %tmp40, 2
%tmp42 = add i32 %tmp41, 1
store i32 %tmp42, i32* %tmp35
%tmp43 = load i32, i32* %tmp33
store i32 %tmp43, i32* %tmp36
br label %while18

while18:
%tmp44 = load i32, i32* %tmp36
%tmp45 = call i32 @plusgrand(i32 %tmp44, i32 2)
%tmp128 = icmp ne i32 %tmp45, 0
br i1 %tmp128, label %do19, label %done20

do19:
%tmp46 = load i32, i32* %tmp35
%tmp47 = call i32 @plusgrandstrict(i32 %tmp46, i32 1)
%tmp48 = icmp ne i32 %tmp47, 0
br i1 %tmp48, label %then7, label %else9

then7:
%tmp67 = load i32, i32* %tmp35
%tmp68 = sub i32 %tmp67, 1
store i32 %tmp68, i32* %tmp35
%tmp69 = load i32, i32* %tmp35
store i32 %tmp69, i32* %tmp37
br label %fi8

else9:
%tmp49 = alloca i32
%tmp52 = load i32*, i32** %tmp31
%tmp51 = getelementptr i32, i32* %tmp52, i32 0
%tmp50 = load i32, i32* %tmp51
store i32 %tmp50, i32* %tmp49
%tmp54 = load i32*, i32** %tmp31
%tmp53 = getelementptr i32, i32* %tmp54, i32 0
%tmp56 = load i32, i32* %tmp36
%tmp57 = sub i32 %tmp56, 1
%tmp59 = load i32*, i32** %tmp31
%tmp58 = getelementptr i32, i32* %tmp59, i32 %tmp57
%tmp55 = load i32, i32* %tmp58
store i32 %tmp55, i32* %tmp53
%tmp60 = load i32, i32* %tmp36
%tmp61 = sub i32 %tmp60, 1
%tmp63 = load i32*, i32** %tmp31
%tmp62 = getelementptr i32, i32* %tmp63, i32 %tmp61
%tmp64 = load i32, i32* %tmp49
store i32 %tmp64, i32* %tmp62
%tmp65 = load i32, i32* %tmp36
%tmp66 = sub i32 %tmp65, 1
store i32 %tmp66, i32* %tmp36
store i32 1, i32* %tmp37
br label %fi8

fi8:
%tmp71 = load i32, i32* %tmp37
%tmp72 = sub i32 %tmp71, 1
%tmp74 = load i32*, i32** %tmp31
%tmp73 = getelementptr i32, i32* %tmp74, i32 %tmp72
%tmp70 = load i32, i32* %tmp73
store i32 %tmp70, i32* %tmp38
%tmp75 = load i32, i32* %tmp36
%tmp76 = load i32, i32* %tmp37
%tmp77 = mul i32 2, %tmp76
%tmp78 = call i32 @plusgrand(i32 %tmp75, i32 %tmp77)
store i32 %tmp78, i32* %tmp39
br label %while15

while15:
%tmp79 = load i32, i32* %tmp39
%tmp122 = icmp ne i32 %tmp79, 0
br i1 %tmp122, label %do16, label %done17

do16:
%tmp80 = alloca i32
%tmp81 = load i32, i32* %tmp37
%tmp82 = mul i32 2, %tmp81
store i32 %tmp82, i32* %tmp80
%tmp83 = load i32, i32* %tmp36
%tmp84 = load i32, i32* %tmp80
%tmp85 = call i32 @plusgrandstrict(i32 %tmp83, i32 %tmp84)
%tmp87 = load i32, i32* %tmp80
%tmp89 = load i32*, i32** %tmp31
%tmp88 = getelementptr i32, i32* %tmp89, i32 %tmp87
%tmp86 = load i32, i32* %tmp88
%tmp91 = load i32, i32* %tmp80
%tmp92 = sub i32 %tmp91, 1
%tmp94 = load i32*, i32** %tmp31
%tmp93 = getelementptr i32, i32* %tmp94, i32 %tmp92
%tmp90 = load i32, i32* %tmp93
%tmp95 = call i32 @plusgrandstrict(i32 %tmp86, i32 %tmp90)
%tmp96 = mul i32 %tmp85, %tmp95
%tmp97 = icmp ne i32 %tmp96, 0
br i1 %tmp97, label %then10, label %fi11

then10:
%tmp98 = load i32, i32* %tmp80
%tmp99 = add i32 %tmp98, 1
store i32 %tmp99, i32* %tmp80
br label %fi11

fi11:
%tmp101 = load i32, i32* %tmp80
%tmp102 = sub i32 %tmp101, 1
%tmp104 = load i32*, i32** %tmp31
%tmp103 = getelementptr i32, i32* %tmp104, i32 %tmp102
%tmp100 = load i32, i32* %tmp103
%tmp105 = load i32, i32* %tmp38
%tmp106 = call i32 @plusgrandstrict(i32 %tmp100, i32 %tmp105)
%tmp107 = icmp ne i32 %tmp106, 0
br i1 %tmp107, label %then12, label %else14

then12:
%tmp108 = load i32, i32* %tmp37
%tmp109 = sub i32 %tmp108, 1
%tmp111 = load i32*, i32** %tmp31
%tmp110 = getelementptr i32, i32* %tmp111, i32 %tmp109
%tmp113 = load i32, i32* %tmp80
%tmp114 = sub i32 %tmp113, 1
%tmp116 = load i32*, i32** %tmp31
%tmp115 = getelementptr i32, i32* %tmp116, i32 %tmp114
%tmp112 = load i32, i32* %tmp115
store i32 %tmp112, i32* %tmp110
%tmp117 = load i32, i32* %tmp80
store i32 %tmp117, i32* %tmp37
%tmp118 = load i32, i32* %tmp36
%tmp119 = load i32, i32* %tmp37
%tmp120 = mul i32 2, %tmp119
%tmp121 = call i32 @plusgrand(i32 %tmp118, i32 %tmp120)
store i32 %tmp121, i32* %tmp39
br label %fi13

else14:
store i32 0, i32* %tmp39
br label %fi13

fi13:
br label %while15

done17:
%tmp123 = load i32, i32* %tmp37
%tmp124 = sub i32 %tmp123, 1
%tmp126 = load i32*, i32** %tmp31
%tmp125 = getelementptr i32, i32* %tmp126, i32 %tmp124
%tmp127 = load i32, i32* %tmp38
store i32 %tmp127, i32* %tmp125
br label %while18

done20:
ret void
}
define i32 @plusgrandstrict(i32 %tmp130, i32 %tmp132) {
%tmp129 = alloca i32
store i32 %tmp130, i32* %tmp129
%tmp131 = alloca i32
store i32 %tmp132, i32* %tmp131
%tmp133 = alloca i32
%tmp134 = alloca i32
%tmp135 = alloca i32
%tmp136 = load i32, i32* %tmp129
%tmp137 = load i32, i32* %tmp131
%tmp138 = mul i32 %tmp136, %tmp137
store i32 %tmp138, i32* %tmp133
%tmp139 = load i32, i32* %tmp129
store i32 %tmp139, i32* %tmp134
%tmp140 = load i32, i32* %tmp131
store i32 %tmp140, i32* %tmp135
br label %while21

while21:
%tmp141 = load i32, i32* %tmp133
%tmp149 = icmp ne i32 %tmp141, 0
br i1 %tmp149, label %do22, label %done23

do22:
%tmp142 = load i32, i32* %tmp135
%tmp143 = sub i32 %tmp142, 1
store i32 %tmp143, i32* %tmp135
%tmp144 = load i32, i32* %tmp134
%tmp145 = sub i32 %tmp144, 1
store i32 %tmp145, i32* %tmp134
%tmp146 = load i32, i32* %tmp134
%tmp147 = load i32, i32* %tmp135
%tmp148 = mul i32 %tmp146, %tmp147
store i32 %tmp148, i32* %tmp133
br label %while21

done23:
%tmp150 = load i32, i32* %tmp134
%tmp151 = icmp ne i32 %tmp150, 0
br i1 %tmp151, label %then24, label %else26

then24:
ret i32 1
br label %fi25

else26:
ret i32 0
br label %fi25

fi25:
ret i32 0
}
define i32 @plusgrand(i32 %tmp153, i32 %tmp155) {
%tmp152 = alloca i32
store i32 %tmp153, i32* %tmp152
%tmp154 = alloca i32
store i32 %tmp155, i32* %tmp154
%tmp156 = alloca i32
%tmp157 = alloca i32
%tmp158 = alloca i32
%tmp159 = load i32, i32* %tmp152
%tmp160 = load i32, i32* %tmp154
%tmp161 = mul i32 %tmp159, %tmp160
store i32 %tmp161, i32* %tmp156
%tmp162 = load i32, i32* %tmp152
store i32 %tmp162, i32* %tmp157
%tmp163 = load i32, i32* %tmp154
store i32 %tmp163, i32* %tmp158
br label %while27

while27:
%tmp164 = load i32, i32* %tmp156
%tmp172 = icmp ne i32 %tmp164, 0
br i1 %tmp172, label %do28, label %done29

do28:
%tmp165 = load i32, i32* %tmp158
%tmp166 = sub i32 %tmp165, 1
store i32 %tmp166, i32* %tmp158
%tmp167 = load i32, i32* %tmp157
%tmp168 = sub i32 %tmp167, 1
store i32 %tmp168, i32* %tmp157
%tmp169 = load i32, i32* %tmp157
%tmp170 = load i32, i32* %tmp158
%tmp171 = mul i32 %tmp169, %tmp170
store i32 %tmp171, i32* %tmp156
br label %while27

done29:
%tmp173 = load i32, i32* %tmp157
%tmp174 = icmp ne i32 %tmp173, 0
br i1 %tmp174, label %then30, label %else32

then30:
ret i32 1
br label %fi31

else32:
%tmp175 = load i32, i32* %tmp158
%tmp176 = icmp ne i32 %tmp175, 0
br i1 %tmp176, label %then33, label %else35

then33:
ret i32 0
br label %fi34

else35:
ret i32 1
br label %fi34

fi34:
br label %fi31

fi31:
ret i32 0
}

