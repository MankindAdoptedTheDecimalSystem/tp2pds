; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.str1 = global [8 x i8] c"Hello, \00"
@.str2 = global [7 x i8] c"world!\00"
@.fmt3 = global [3 x i8] c"%s\00"


define void @main() {
%tmp1 = getelementptr [8 x i8], [8 x i8]* @.str1, i64 0, i32 0
%tmp2 = getelementptr [7 x i8], [7 x i8]* @.str2, i64 0, i32 0
%tmp5 = call i32 @strlen(i8* %tmp1)
%tmp4 = call i32 @strlen(i8* %tmp2)
%tmp7 = add i32 %tmp5, %tmp4
%tmp6 = add i32 %tmp7, 1
%tmp3 = call i8* @malloc(i32 %tmp6)
call void @strcpy(i8* %tmp3, i8* %tmp1)
%tmp8 = getelementptr i8, i8* %tmp3, i32 %tmp5
call void @strcpy(i8* %tmp8, i8* %tmp2)
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt3, i64 0, i64 0), i8* %tmp3)
ret void
}

