; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.str1 = global [6 x i8] c"world\00"
@.str2 = global [8 x i8] c"Hello, \00"
@.str3 = global [2 x i8] c"!\00"
@.fmt4 = global [7 x i8] c"%s%s%s\00"


define void @main() {
%tmp1 = alloca i8*
%tmp2 = getelementptr [6 x i8], [6 x i8]* @.str1, i64 0, i32 0
store i8* %tmp2, i8** %tmp1
%tmp3 = getelementptr [8 x i8], [8 x i8]* @.str2, i64 0, i32 0
%tmp4 = load i8*, i8** %tmp1
%tmp5 = getelementptr [2 x i8], [2 x i8]* @.str3, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.fmt4, i64 0, i64 0), i8* %tmp3, i8* %tmp4, i8* %tmp5)
ret void
}

