; Target
target triple = "x86_64-unknown-linux-gnu"
; External declarations
declare i32 @printf(i8* noalias nocapture, ...)
declare i32 @scanf(i8* noalias nocapture, ...)
declare i8* @malloc(i32)
declare i32 @strlen(i8* noalias nocapture)
declare void @strcpy(i8* noalias nocapture, i8* noalias nocapture)
declare void @sprintf(i8* noalias nocapture, i8* noalias nocapture, ...)

; Actual code begins
@.str1 = global [15 x i8] c"Hello from f: \00"
@.str2 = global [2 x i8] c"\0A\00"
@.fmt3 = global [7 x i8] c"%s%s%s\00"
@.str4 = global [12 x i8] c"String in f\00"
@.str5 = global [3 x i8] c"hi\00"
@.fmt6 = global [3 x i8] c"%s\00"


define i8* @f(i8* %tmp2) {
%tmp1 = alloca i8*
store i8* %tmp2, i8** %tmp1
%tmp3 = getelementptr [15 x i8], [15 x i8]* @.str1, i64 0, i32 0
%tmp4 = load i8*, i8** %tmp1
%tmp5 = getelementptr [2 x i8], [2 x i8]* @.str2, i64 0, i32 0
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.fmt3, i64 0, i64 0), i8* %tmp3, i8* %tmp4, i8* %tmp5)
%tmp6 = getelementptr [12 x i8], [12 x i8]* @.str4, i64 0, i32 0
ret i8* %tmp6
ret i8* null
}
define void @main() {
%tmp7 = getelementptr [3 x i8], [3 x i8]* @.str5, i64 0, i32 0
%tmp8 = call i8* @f(i8* %tmp7)
call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.fmt6, i64 0, i64 0), i8* %tmp8)
ret void
}

